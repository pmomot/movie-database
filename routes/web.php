<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('movie.index');
});

Route::get('/movie', 'MovieController@index')->name('movie.index');
Route::get('/movie/new', 'MovieController@create')->name('movie.create');
Route::get('/movie/{movie}', 'MovieController@show')->name('movie.show');
Route::get('/movie/{movie}/edit', 'MovieController@edit')->name('movie.edit');
Route::put('/movie/{movie}', 'MovieController@update')->name('movie.update');
Route::post('/movie', 'MovieController@store')->name('movie.store');

Route::get('/movie-genere/{movie}', 'MovieGenereController@index')->name('movie-genere.index');
Route::get('/movie-genere/{movie}/new', 'MovieGenereController@create')->name('movie-genere.create');
Route::post('/movie-genere/{movie}', 'MovieGenereController@store')->name('movie-genere.store');
Route::get('/movie-genere/{movie}/{genere}', 'MovieGenereController@edit')->name('movie-genere.edit');
Route::delete('/movie-genere/{movie}/{genere}', 'MovieGenereController@destroy')->name('movie-genere.destroy');

Route::get('/movie-country/{movie}', 'MovieCountryController@index')->name('movie-country.index');
Route::post('/movie-country/{movie}', 'MovieCountryController@store')->name('movie-country.store');
Route::get('/movie-country/{movie}/new', 'MovieCountryController@create')->name('movie-country.create');
Route::get('/movie-country/{movie}/{country}', 'MovieCountryController@edit')->name('movie-country.edit');
Route::delete('/movie-country/{movie}/{country}', 'MovieCountryController@destroy')->name('movie-country.destroy');

Route::get('/person', 'PersonController@index')->name('person.index');
Route::get('/person/new', 'PersonController@create')->name('person.create');
Route::get('/person/{person}', 'PersonController@show')->name('person.show');
Route::post('/person', 'PersonController@store')->name('person.store');
Route::get('/person/{person}/edit', 'PersonController@edit')->name('person.edit');
Route::put('/person/{person}', 'PersonController@update')->name('person.update');
Route::delete('/person/{person}', 'PersonController@destroy')->name('person.destroy');

Route::get('/performance/{movie}/new', 'PerformanceController@selectActor')->name('performance.selectActor');
Route::get('/performance/{movie}/{person}/new', 'PerformanceController@create')->name('performance.create');
Route::post('/performance/{movie}/{person}/', 'PerformanceController@store')->name('performance.store');
Route::get('/performance/{performance}/edit', 'PerformanceController@edit')->name('performance.edit');
Route::delete('/performance/{performance}', 'PerformanceController@destroy')->name('performance.destroy');
