<?php

$countries = [];
$countries['USA'] = new \App\Country;
$countries['USA']->name = 'USA';
$countries['Polska'] = new \App\Country;
$countries['Polska']->name = 'Polska';
$countries['Wielka Brytania'] = new \App\Country;
$countries['Wielka Brytania']->name = 'Wielka Brytania';
$countries['Nowa Zelandia'] = new \App\Country;
$countries['Nowa Zelandia']->name = 'Nowa Zelandia';
foreach ($countries as $c) { $c->save(); }

$mark_hamill = new \App\Person;
$mark_hamill->name = 'Mark Hamill';
$mark_hamill->birth_date = '1951-09-25';
$mark_hamill->height = 175;
$mark_hamill->country()->associate($countries['USA'])->save();

# 'associate()' tylko dla relacji 'BelongsTo'
# + 'save()'

$harrison_ford = new \App\Person;
$harrison_ford->name = 'Harrison Ford';
$harrison_ford->birth_date = '1942-07-13';
$harrison_ford->birth_city = 'Chicago, Illinois';
$harrison_ford->height = 185;
$harrison_ford->bio = 'Harrison Ford jest jednym z najpopularniejszych gwiazdorów światowego kina. Dorastał na przedmieściach Chicago. Dla aktorstwa zrezygnował z nauki w Ripon College. Zadebiutował w filmie "Spryciarz Ed". Po pierwszych niepowodzeniach zawodowych zajął się stolarstwem, które do dziś stanowi jego hobby.';
$harrison_ford->country()->associate($countries['USA'])->save();

$generes = [];
$generes['Przygodowy'] = new \App\Genere;
$generes['Przygodowy']->name = 'Przygodowy';
$generes['Sci-Fi'] = new \App\Genere;
$generes['Sci-Fi']->name = 'Sci-Fi';
$generes['Fantasy'] = new \App\Genere;
$generes['Fantasy']->name = 'Fantasy';
foreach ($generes as $g) { $g->save(); }

$movies = [];
$movies['Star Wars'] = new \App\Movie;
$movies['Star Wars']->title = "Gwiezdne Wojny: Część IV - Nowa Nadzieja";
$movies['Star Wars']->release_date = "1977-05-25";
$movies['Star Wars']->summary = 'Złowrogie Imperium zawładnęło galaktyką. Uwięzionej przez Dartha Vadera księżniczce Lei z nieoczekiwaną pomocą przyjdą kosmiczny przemytnik Han Solo i młody Luke Skywalker.';
$movies['Star Wars']->save();
$movies['Star Wars']->generes()->saveMany([$generes['Przygodowy'], $generes['Sci-Fi']]);
$movies['Star Wars']->countries()->save($countries['USA']);

$elijah_wood = \App\Person::create([
    'name' => 'Elijah Wood',
    'birth_date' => '1981-01-28',
    'birth_city' => 'Cedar Rapids, Iowa',
    'height' => 168,
    'bio' => 'W świat show businessu wysłała go mama, która pewnego razu, oglądając telewizyjną reklamę, stwierdziła, że jej syn mógłby przecież w niej występować – był uroczym dzieckiem, w szkole podstawowej śpiewał w chórze, grał w szkolnych przedstawieniach.',
]);
$elijah_wood->country()->associate($countries['USA'])->save();

$sean_astin = \App\Person::create([
    'name' => 'Sean Astin',
    'birth_date' => '1971-02-25',
    'birth_city' => 'Santa Monica, Kalifornia',
    'height' => 170,
    'bio' => 'Skończył Crossroad High School for the Arts. Zadebiutował w wieku 8 lat w filmie telewizyjnym "Please Don\'t Hit Me Mom". Wystąpił tam razem ze swoją matką, Patty Duke. Jego ojciec, John Astin, jest również aktorem, dzięki czemu Sean od najmłodszych lat miał bardzo dobry kontakt z filmem.',
]);
$sean_astin->country()->associate($countries['USA'])->save();

$movies['LotR 1'] = \App\Movie::create([
    'title' => "Władca Pierścieni: Drużyna Pierścienia",
    'release_date' => "2001-12-10",
    'summary' => 'Podróż hobbita z Shire i jego ośmiu towarzyszy, której celem jest zniszczenie potężnego pierścienia pożądanego przez Czarnego Władcę - Saurona.',
]);
$movies['LotR 1']->generes()->saveMany([$generes['Przygodowy'], $generes['Fantasy']]);
$movies['LotR 1']->countries()->saveMany([$countries['Nowa Zelandia'], $countries['USA']]);

$movies['LotR 2'] = \App\Movie::create([
    'title' => 'Władca Pierścieni: Dwie Wieże',
    'release_date' => '2002-12-05',
    'summary' => 'Drużyna Pierścienia zostaje rozbita, lecz zdesperowany Frodo za wszelką cenę chce wypełnić powierzone mu zadanie. Aragorn z towarzyszami przygotowuje się, by odeprzeć atak hord Sarumana.',
]);
$movies['LotR 2']->generes()->saveMany([$generes['Przygodowy'], $generes['Fantasy']]);
$movies['LotR 2']->countries()->saveMany([$countries['Nowa Zelandia'], $countries['USA']]);

$movies['LotR 3'] = \App\Movie::create([
    'title' => 'Władca Pierścieni: Powrót króla',
    'release_date' => '2003-12-01',
    'summary' => 'Zwieńczenie filmowej trylogii wg powieści Tolkiena. Aragorn jednoczy siły Śródziemia, szykując się do bitwy, która ma odwrócić uwagę Saurona od podążających w kierunku Góry Przeznaczenia hobbitów.',
]);
$movies['LotR 3']->generes()->saveMany([$generes['Przygodowy'], $generes['Fantasy']]);
$movies['LotR 3']->countries()->saveMany([$countries['Nowa Zelandia'], $countries['USA']]);

$robin_williams = \App\Person::create([
    'name' => 'Robin Williams',
    'birth_date' => '1951-06-21',
    'birth_city' => 'Chicago, Illinois',
    'death_date' => '2014-07-11',
    'height' => 170,
    'bio' => 'Studiował nauki polityczne. Dopiero później odkrył w sobie talent komediowy i postanowił zostać aktorem. Uznanie publiczności najpierw zdobył w telewizyjnym serialu komediowym "Happy Days" rolą kosmity Morka z Ork.',
]);
$robin_williams->country()->associate($countries['USA'])->save();

$przygodowy = $generes['Przygodowy'];
$scifi = $generes['Sci-Fi'];
$fantasy = $generes['Fantasy'];
$animowany = \App\Genere::create(['name' => 'Animowany']);
$dokumentalny = \App\Genere::create(['name' => 'Dokumentalny']);
$dramat = \App\Genere::create(['name' => 'Dramat']);
$familijny = \App\Genere::create(['name' => 'Familijny']);
$gangsterski = \App\Genere::create(['name' => 'Gangsterski']);
$horror = \App\Genere::create(['name' => 'Horror']);
$komedia = \App\Genere::create(['name' => 'Komedia']);
$kryminal = \App\Genere::create(['name' => 'Kryminał']);
$komedia_romantyczna = \App\Genere::create(['name' => 'Komedia Romantyczna']);
$psychologiczny = \App\Genere::create(['name' => 'Psychologiczny']);
$thriller = \App\Genere::create(['name' => 'Thriller']);
$wojenny = \App\Genere::create(['name' => 'Wojenny']);

$polska = $countries['Polska'];
$usa = $countries['USA'];
$uk = $countries['Wielka Brytania'];
$nowa_zelandia = $countries['Nowa Zelandia'];
$chiny = \App\Country::create(['name' => 'Chiny']);
$francja = \App\Country::create(['name' => 'Francja']);
$hiszpania = \App\Country::create(['name' => 'Hiszpania']);
$iran = \App\Country::create(['name' => 'Iran']);
$japonia = \App\Country::create(['name' => 'Japonia']);
$niemcy = \App\Country::create(['name' => 'Niemcy']);
$rosja = \App\Country::create(['name' => 'Rosja']);
$wlochy = \App\Country::create(['name' => 'Włochy']);

$matrix = \App\Movie::create([
    'title' => 'Matrix',
    'release_date' => '1994-03-24',
]);

$blade_runner = \App\Movie::create([
    'title' => 'Łowca Androidów',
    'release_date' => '1982-06-25',
    'summary' => 'Rok 2019. Rick Deckard jako członek specjalnego oddziału policji tropi i eliminuje replikantów - istoty stworzone za pomocą biotechnologii do wykonywania niebezpiecznych zadań.',
]);
$blade_runner->countries()->saveMany([$usa]);
$blade_runner->generes()->saveMany([$scifi, $dramat]);
$blade_runner->save();

$lemonny_snicket = \App\Movie::create([
    'title' => 'Lemony Snicket: Seria niefortunnych zdarzeń',
    'release_date' => '2004-12-16',
    'summary' => 'Po śmierci rodziców, trójka rodzeństwa dostaje się pod opiekę hrabiego Olafa - aktora teatralnego i ich kuzyna, planującego ukraść otrzymaną w spadku przez dzieci fortunę.',
]);
$lemonny_snicket->countries()->saveMany([$usa, $niemcy]);
$lemonny_snicket->generes()->saveMany([$familijny, $przygodowy]);
$lemonny_snicket->save();

$leon_zawodowiec = \App\Movie::create([
    'title' => 'Leon Zawodowiec',
    'release_date' => '1994-09-14',
    'summary' => 'Płatny morderca ratuje dwunastoletnią dziewczynkę, której rodzina została zabita przez skorumpowanych policjantów.',
]);
$leon_zawodowiec->countries()->saveMany([$francja]);
$leon_zawodowiec->generes()->saveMany([$dramat, $kryminal]);
$leon_zawodowiec->save();

$ojciec_chrzestny = \App\Movie::create([
    'title' => 'Ojciec Chrzestny',
    'release_date' => '1972-03-15',
    'summary' => 'Opowieść o nowojorskiej rodzinie mafijnej. Starzejący się Don Corleone pragnie przekazać władzę swojemu synowi.',
]);
$ojciec_chrzestny->countries()->saveMany([$usa]);
$ojciec_chrzestny->generes()->saveMany([$dramat, $gangsterski]);
$ojciec_chrzestny->save();

$gladiator = \App\Movie::create([
    'title' => 'Gladiator',
    'release_date' => '2000-05-01',
    'summary' => 'Generał Maximus - prawa ręka cesarza, szczęśliwy mąż i ojciec - w jednej chwili traci wszystko. Jako niewolnik-gladiator musi walczyć na arenie o przeżycie.',
]);
$gladiator->countries()->saveMany([$usa, $uk]);
$gladiator->generes()->saveMany([$dramat]);
$gladiator->save();

$obcy = \App\Movie::create([
    'title' => 'Obcy - 8. pasażer Nostromo',
    'release_date' => '1979-05-25',
    'summary' => 'Załoga statku kosmicznego Nostromo odbiera tajemniczy sygnał i ląduje na niewielkiej planetoidzie, gdzie jeden z jej członków zostaje zaatakowany przez obcą formę życia.',
]);
$obcy->countries()->saveMany([$usa]);
$obcy->generes()->saveMany([$horror, $scifi]);
$obcy->save();

$hot_fuzz = \App\Movie::create([
    'title' => 'Hot Fuzz - Ostre Psy',
    'release_date' => '2007-02-14',
    'summary' => 'Policjant z Londynu zostaje przeniesiony do małego miasta. Wraz ze swoim nowym partnerem próbują wyjaśnić serię podejrzanych wypadków i zdarzeń.',
]);
$hot_fuzz->countries()->saveMany([$francja, $uk]);
$hot_fuzz->generes()->saveMany([$komedia]);
$hot_fuzz->save();

$zakochany_bez_pamieci = \App\Movie::create([
    'title' => 'Zakochany bez pamięci',
    'release_date' => '2004-03-19',
    'summary' => 'Joel odkrywa, że jego była dziewczyna poddała się zabiegowi wymazania go z pamięci. On postanawia zrobić to samo, ale w trakcie operacji uświadamia sobie, że wciąż ją kocha.',
]);
$zakochany_bez_pamieci->countries()->saveMany([$usa]);
$zakochany_bez_pamieci->generes()->saveMany([$komedia_romantyczna, $dramat, $scifi]);
$zakochany_bez_pamieci->save();

$jumanji = \App\Movie::create([
    'title' => 'Jumanji',
    'release_date' => '1995-12-15',
    'summary' => 'Dwunastoletni Alan wraz z koleżanką rozpoczyna zabawę w tajemniczą grę Jumanji, która wciąga go do dżungli. Po latach zostaje uwolniony przez dwójkę rodzeństwa, Judy i Petera.',
]);
$jumanji->countries()->saveMany([$usa]);
$jumanji->generes()->saveMany([$fantasy, $przygodowy]);
$jumanji->save();

// $new_actor = \App\Person::create([
//     'name' => '',
//     'birth_date' => '',
//     'birth_city' => '',
//     'death_date' => null,
//     'height' => 0,
//     'bio' => '',
// ]);
