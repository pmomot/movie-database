<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function movies()
    {
        return $this->belongsToMany('App\Movie', 'country_movie', 'movie_id',
            'country_id');
    }

    public function persons()
    {
        return $this->hasMany('App\Person');
    }
}
