<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Country;

class StorePersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:70',
            'birth_date' => 'nullable|date_format:Y-m-d',
            'death_date' => 'nullable|date_format:Y-m-d',
            'birth_city' => 'nullable|min:3|max:40',
            'height' => 'nullable|integer|min:100|max:300',
            'bio' => 'nullable|max:2048',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => 'Nazwisko',
            'birth_date' => 'Data Urodzenia',
            'death_date' => 'Data Śmierci',
            'birth_city' => 'Miejsce Urodzenia',
            'height' => 'Wzrost',
            'bio' => 'Notka Biograficzna',
        ];
    }
}
