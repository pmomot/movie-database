<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Country;

class MovieCountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function index(Movie $movie)
    {
        return view('movie-country.index', compact('movie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function create(Movie $movie)
    {
        $attached = $movie->countries->pluck('id')->all();
        $remaining = Country::all()->except($attached);
        return view('movie-country.create', ['movie' => $movie, 'countries' => $remaining]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Movie $movie)
    {
        $country = Country::find($request->input('country_id'));
        if (!$country || $movie->belongsToCountry($country)) {
            return redirect()->back();
        }
        $movie->countries()->attach($country);
        return redirect()->route('movie-country.index', compact('movie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie    $movie
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie, Country $country)
    {
        abort_if(!$movie->belongsToCountry($country), 404);
        return view('movie-country.edit', compact('movie', 'country'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie    $movie
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie, Country $country)
    {
        abort_unless($movie->belongsToCountry($country), 404);
        $movie->countries()->detach($country);
        return redirect()->route('movie-country.index', compact('movie'));
    }
}
