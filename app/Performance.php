<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property  \App\Person  $actor
 * @property  \App\Movie   $movie
 * @property  string       $character
 */
class Performance extends Model
{
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['character'];

    public function actor()
    {
        return $this->belongsTo('App\Person', 'person_id');
    }

    public function movie()
    {
        return $this->belongsTo('App\Movie');
    }
}
