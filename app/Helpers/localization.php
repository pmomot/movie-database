<?php

namespace App\Helpers;

function localizeDate(\DateTime $date)
{
    $months = [null, 'stycznia', 'lutego', 'marca',
        'kwietnia', 'maja', 'czerwca', 'lipca', 'sierpnia',
        'września', 'października', 'listopada', 'grudnia'];

    $year = $date->format('Y');
    $monthNumber = $date->format('n');
    $month = $months[$monthNumber];
    $day = $date->format('j');
    return "$day $month $year";
}

function localizeAge(int $age): string
{
    $text = "$age lat";
    $last_digit = $age % 10;
    if (in_array($last_digit, [2, 3, 4])) {
        $text .= "a";
    }
    return $text;
}
