<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

/**
 * @property  \Illuminate\Database\Eloquent\Collection  $cast
 */
class Movie extends Model
{
    use Searchable;

    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'release_date', 'summary'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'release_date' => 'date',
    ];

    public function cast()
    {
        return $this->hasMany('App\Performance');
    }

    public function countries()
    {
        return $this->belongsToMany('App\Country', 'country_movie', 'country_id',
            'movie_id');
    }

    public function generes()
    {
        return $this->belongsToMany('App\Genere');
    }

    /**
     * Checks if the movie has been assigned to a genere.
     * 
     * @param  \App\Genere  $genere
     * @return bool
     */
    public function belongsToGenere(Genere $genere)
    {
        return (bool) $this->generes->contains($genere);
    }

    /**
     * Checks if the movie has been assigned to a country.
     * 
     * @param  \App\Country  $country
     * @return bool
     */
    public function belongsToCountry(Country $country)
    {
        return (bool) $this->countries->contains($country);
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        $array['release_date'] = (int) $this->release_date->format('Ymd');

        return $array;
    }

    /**
     * Browse (with or without search query) all movies,
     * refering to the search indecies, if required.
     *
     * @param  string  $query
     * @param  string  $sortAttr  Attribute to sort by.
     * @param  string  $sortDir   'asc' or 'desc'
     * @param  int     $perPage
     * @return Illuminate\Pagination\LengthAwarePaginator
     */
     static public function browse(?string $query,
	     string $sortAttr, string $sortDir, int $perPage)
    {
        if ($query == '') {
            return static::browseAll($sortAttr, $sortDir, $perPage);
        }
        else {
            return static::browseMatching($query, $sortAttr,
                $sortDir, $perPage);
        }
    }

    static private function browseMatching($query, $sortAttr,
        $sortDir, $perPage)
    {
        if ($sortAttr == 'release_date') {
            switch ($sortDir) {
            case 'asc':
                return Movie::search($query)
                    ->within(static::dateAscIndexName())
                    ->paginate($perPage);
            case 'desc':
                return Movie::search($query)
                    ->within(static::dateDescIndexName())
                    ->paginate($perPage);
            }
        }
        return Movie::search($query)->paginate($perPage);
    }

    static private function browseAll($sortAttr, $sortDir, $perPage)
    {
        if ($sortAttr == '') {
            return Movie::paginate($perPage);
        }
        return Movie::orderBy($sortAttr, $sortDir)->paginate($perPage);
    }

    static private function dateAscIndexName()
    {
        return config('app_indecies.movie.release_date.asc');
    }

    static private function dateDescIndexName()
    {
        return config('app_indecies.movie.release_date.desc');
    }
}

