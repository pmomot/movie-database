# Webowy katalog filmów

Aplikacja napisana w PHP/Larvel na potrzeby projektu inżynierskiego.


## Instalacja

### Pobranie zależności

Pierwszym krokiem jest pobranie bibliotek wykorzystywanych przez aplikację.


Do pobrania bibliotek potrzebny jest program
[composer](https://getcomposer.org).

Żeby pobrać wymagane biblioteki, należy:

1. Przejść do wiersza poleceń systemu operacyjnego.
2. Uczynić katalog główny aplikacji katalogiem bieżącym powłoki systemowej.  
   ```
   $ cd <ścieżka_do_katalogu>
   ```
3. Wykonać polecenie:  
   ```
   $ composer install
   ```


### Konfiguracja platformy Laravel


Konieczne jest utworzenie pliku, w którym będą zadeklarowane zmienne
środowiskowe potrzebne do działania platformy Laravel.  Plik musi być
umieszczony w głównym katalogu projektu i nosić nazwę `.env`
(*sic!*).  Autor pracy sugeruje w pierwszej kolejności wykorzystanie
zawartości przykładowego pliku konfiguracyjnego, który nazywa się
`.env.example`.  Sprowadza się to do wykonania następującego polecenia
powłoki wewnątrz katalogu głównego projektu:

```
$ cp .env.example .env
```

Framework Laravel szyfruje pliki związane ze sesjami użytkowników aplikacji.
Wykorzystuje w tej operacji wartość klucza, która jest
zadeklarowanego właśnie wewnątrz pliku `.env`.  Wartość klucza można
zadeklarować samodzielnie - przez edycję pliku w edytorze tekstu - lub przez
wywołanie następującego programu:

```
php artisan key:generate
```

Kolejnym krokiem do uruchomienia serwera aplikacji jest skonfigurowanie
połączenia z bazą danych.


### Połączenie z bazą danych

Konfiguracji bazy danych używanej przez aplikację dokonuje się przez przypisanie
odpowiednich wartości zmiennym środowiskowym, czyli przez edycję pliku
`.env`.  Nazwy tych zmiennych zaczynają się od przedrostka `DB_` .

Twórca aplikacji korzystał z systemu bazodanowego SQLite, ale użytkownik może
wykorzystać dowolny system bazodanowy wspierany przez platformę Laravel, np.
MySQL, PostgreSQL lub SQL Server.

Przykładowy proces konfiguracji aplikacji do pracy z bazą danych SQLite, może
przebiegać w następujący sposób:

1. Usunąć wszystkie wiersze zaczynające się od `DB_` w pliku
   `.env` .
2. Dodać następujący wiersz do pliku \filepath{.env}:  
   ```
   DB_CONNECTION=sqlite
   ```
3. Utworzyć pusty plik o ścieżce `database/database.sqlite` względem katalogu
   projektu.  Jeżeli korzystamy z systemu zgodnego z POSIX (np. Linux lub macOS)
   taki plik można utworzyć poleceniem:  
   ```
   touch database/database.sqlite
   ```
   Na komputerze pod kontrolą systemu operacyjnego MS Windows można to zrobić
   następującym poleceniem:  
   ```
   type NUL >database\database.sqlite
   ```
4. Utworzyć wymaganą przez aplikację strukturę bazy danych przez wykonanie
   polecenia:  
   ```
   php artisan migrate
   ```

Opcjonalnie użytkownik może też dodać zbiór przykładowych wierszy do bazy danych
za pomocą skryptu stworzonego przez twórcę aplikacji.  Skrypt można wykonać
poleceniem:

```
php artisan populate_database
```

Plik z bazą danych można umieścić w innym katalogu, ale należy wtedy
zadeklarować ścieżkę do niej w zmiennej środowiskowej `DB_DATABASE` .
Domyślną ścieżką względną do bazy danych jest `database/database.sqlite` .

**Uwaga!**
Zadeklarowanie zmiennej `DB_DATABASE` na pustą wartość nie spowoduje, że
aplikacja będzie szukała się pliku z bazą pod domyślną ścieżką.  Tylko usunięcie
wiersza z deklaracją tej zmiennej wymusi domyślne zachowanie.


### Konfiguracja indeksu przeszukiwania

Aplikacja korzysta z chmury obliczeniowej (SaaS, *Software as a Service*)
[Algolia](https://www.algolia.com) do realizacji przeszukiwania katalogu filmów
i osób kina.  Jest to serwis oferujący zarządzanie indeksami wyszukiwania oraz
API webowe do korzystania z własnościowego algorytmu przeszukiwania tych
indeksów.  Żeby móc z niej skorzystać należy utworzyć konto w serwisie
internetowym.

Pierwszym krokiem do skonfigurowania połączenia z chmurą obliczeniową polega na
skopiowaniu do zmiennych środowiskowych klucza dostępu do API oraz ID
przypisanego do konta .  Wartości te można znaleźć na stronie serwisu, w
zakładce *API Keys*.

W pliku `.env` należy zadeklarować następujące zmienne środowiskowe:

```
ALGOLIA_APP_ID=<Application ID>
ALGOLIA_SECRET=<Admin API Key>
```

Następnie trzeba należy zdefiniować cztery indeksy przeszukiwań.

Pierwszym indeksem służy do przeszukiwania kolekcji filmów.  Żeby go utworzyć
należy przejść do zakładki *Indecies* i kliknąć w przycisk *Create Index*.  W
otwartym oknie dialogowym należy wprowadzić dowolną nazwę dla indeksu (np.
`movies`) i zatwierdzić.

Następnie należy wybrać ten indeks z listy indeksów.  Na otwartej podstronie
otworzyć zakładkę *Configuration* i dodać dwa atrybuty, które mają być
brane pod uwagę przy przeszukiwaniu: `title` oraz `summary`.

Drugi indeks, który należy utworzyć, również będzie służył do szukania filmów,
ale wyniki będą posortowane w porządku nierosnącym względem daty premiery filmu.
Żeby go utworzyć należy otworzyć zakładkę *Replicas* na podstronie
poprzedniego indeksu i kliknąć przycisk opisany jako *Create Replica Index*.  Tworzonemu indeksowi można nadać dowolną nazwę.  Na potrzeby
instrukcji, założono, że indeks nosi nazwę `movie_date_desc`.

W kolejnym kroku należy przejść do podstrony nowo utworzonego indeksu, otworzyć
zakładkę *Configuration/Ranking and Searching* i dodać nowy
atrybut sortowania za pomocą przycisku *Add sort-by attribute*.  Atrybutowi
trzeba nadać nazwę `release_date`, a potem z listy wyboru obok tej nazwy
wybrać opcję *Descending*.

Trzeci indeks również posłuży do przeszukiwaniu filmów, tylko zwracane wyniki
będą posortowane w kolejności przeciwnej, do tych pozyskanych z drugiego
indeksu.  Tworzy się go tak samo jak poprzedni indeks, tylko z listy wyboru
wybrać należy opcję *Ascending*.  Indeksowi można nadać dowolną nazwę.  Na
potrzeby dalszego opisu założono, że nadano mu nazwę `movie_date_asc`.

Ostatni indeks posłuży do przeszukiwania kolekcji wpisów o osobach filmu.  Żeby
go utworzyć, należy przejść do zakładki *Indecies*, kliknąć w przycisk
opisany jako *Create Index*.  W otwartym oknie dialogowym można wybrać
dowolną nazwę dla indeksu.  Do końca tego podrozdziału będziemy się do niego
odnosić przy pomocy nazwy `person_index`.

Gdy ostatni indeks zostanie utworzony, należy otworzyć jego podstronę i w
zakładce *Configuration/Searchable Attributes* dodać atrybuty `name` oraz
`birth_city`.

Ostatnim krokiem jest przypisanie nazw utworzonych indeksów do następujących
zmiennych środowiskowych wewnątrz pliku pliku `.env`.

```
MOVIE_INDEX_NAME=movie
MOVIE_RELEASE_DATE_ASC_INDEX_NAME=movie_date_asc
MOVIE_RELEASE_DATE_DESC_INDEX_NAME=movie_date_desc
PERSON_INDEX_NAME=person
```

Jeżeli baza danych zawierała już jakieś wpisy, można je zapisać w indeksie wyszukiwania wykonując następujące polecenia:

```
php artisan scout:import 'App\Movie'
php artisan scout:import 'App\Person'
```


### Serwowanie przez deweloperski serwer HTTP

Żeby uruchomić aplikację na serwerze zintegrowanym z interpreterem języka PHP, wystarczy wykonać następujące polecenie, gdy katalogiem bieżącym wiersza poleceń jest katalog aplikacji:

```
$ php artisan serve
```


## Problem z Konfiguracją SQLite

Plik `.env` przed rozwiązaniem problemu:

```
DB_CONNECTION=sqlite
DB_HOST=
DB_DATABASE=
DB_PORT=
DB_USERNAME=root
DB_PASSWORD=
```

W pliku `config/database.php`:

```php
'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],
```

Problem:

```
movie-database$ php artisan migrate:fresh
Illuminate\Database\QueryException : SQLSTATE[HY000] [14] unable to open
database file (SQL: PRAGMA foreign_keys = ON;)
...
```

Problemem jest obecność wiersza `DB_DATABASE=` w pliku `.env`. Brak wartość
po `DB_DATABASE` nie oznacza, że do stałej nie jest przypisana żadna wartość,
tylko że przypisany jest pusty ciąg znaków. Żeby przypisana została domyślna
wartość `database_path('database.sqlite')`, trzeba usunąć ten wiersz z pliku.

Trzeba utworzyć 

Plik `.env` poprawnie skonfgurowany.

```

DB_CONNECTION=sqlite

```


## Jak aktywować rozszerzenie SQLite w PHP na Linuxie?

Jeżeli przy próbie wykonania polecenia `php artisan migrate` Laravel
rzuca wyjątek:

```
[Illuminate\Database\QueryException] could not find driver
[PDOException] could not find driver
```

onacza to, że nie skonfigurowałeś rozszrzenia `php-sqlite`.

Pobierz rozszrzenie `php-sqlite` z repozytorium dystrybucji systemu,
a potem w pliku konfiguracji php (`/etc/php/php.ini` na Arch Linux)
odkomentuj ten wiersz:

```
;extension=sqlite.so
```

