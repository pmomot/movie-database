@extends('layout')

@section('content')
<div class="row justify-center">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Dodawanie nowej roli do filmu
            </div>
            <div class="card-body">
                
                <form method="POST" action="{{ route('performance.store', compact('movie', 'person')) }}" class="mb-0">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-4 text-md-right">Film</div>
                        <div class="col-md-6 text-md text-truncate">{{ $movie->title }}</div>
                    </div> {{-- .row --}}
                    <div class="form-group row">                        
                        <div class="col-md-4 text-md-right">Odtwórca</div>
                        <div class="col-md-6 text-md text-truncate">{{ $person->name }}</div>
                    </div> {{-- .form-group.row --}}
                    <div class="form-group row">                        
                        <label for="character" class="col-md-4 col-form-label text-md-right">Postać</label>
                        <div class="col-md-6">
                            <input name="character" type="text"
                                   class="form-control @error('character') is-invalid @enderror"
                                   value="{{ old('character') }}">
                            @error('character')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <div class="col-md-3 offset-md-4">
                            <button type="submit" class="btn btn-primary">Dodaj</button>
                            <a class="btn btn-link" href="{{ route('performance.selectActor', compact('movie')) }}">Wróć</a>
                        </div> {{-- .col-md-8.offset-md-4 --}}
                    </div> {{-- .form-group.row --}}
                </form>
            </div> {{-- .card-body --}}
        </div> {{-- .card --}}
    </div> {{-- .col-12.col-lg-8 --}}
</div> {{-- .row --}}
@endsection
