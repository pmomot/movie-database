@extends('layout')

@section('content')
<div class="row justify-center">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Edytuj film
            </div>
            <div class="card-body">
                <p>Czy na pewno chcesz usunąć ten kraj z listy?</p>
            <form method="POST" action="{{ route('movie-country.destroy', compact('movie', 'country')) }}" class="mb-0">
                    @csrf
                    @method('DELETE')

                    <div class="form-group row">
                        <div class="col-md-4 text-md-right">Film</div>
                        <div class="col-md-6 text-md text-truncate">{{ $movie->title }}</div>
                    </div> {{-- .row --}}
                    <div class="form-group row">
                        <div class="col-md-4 text-md-right">Kraj</div>
                        <div class="col-md-6 text-md text-truncate">{{ $country->name }}</div>
                    </div> {{-- .row --}}

                    <div class="form-group row">
                        <div class="col-md-3 offset-md-4">
                            <button type="submit" class="btn btn-danger">Usuń</button>
                            <a class="btn btn-light" href="{{ route('movie-country.index', compact('movie')) }}">Wróć</a>
                        </div> {{-- .col-md-8.offset-md-4 --}}
                    </div> {{-- .form-group.row --}}
                </form>
            </div> {{-- .card-body --}}
        </div> {{-- .card --}}
    </div> {{-- .col-12.col-lg-8 --}}
</div> {{-- .row --}}
@endsection
