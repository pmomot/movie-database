@extends('layout')

@section('content')
<div class="row">
    <div class="col-12">
        <h1 class="mb-3">Ludzie filmu</h1>
        <p><a href="{{ route('person.create') }}" class="btn btn-outline-primary">Dodaj osobę</a></p>
        <p>
        @if(! $people)
            Baza osób jest pusta.
        @else
            <div class="list-group">
                @foreach($people as $person)
            <a class="list-group-item list-group-item-action" href="{{ route('person.show', compact('person')) }}">
                        <p class="font-weight-bolder">{{ $person->name }}</p>
                        <p><table>
                            <tr class="align-top">
                                <td class="text-muted">Wiek:</td>
                                <td class="pl-4">@include('person.age-inline')</td>
                            </tr>
                            <tr class="align-top">
                                <td class="text-muted">Pochodzenie:</td>
                                <td class="pl-4">@include('person.birth-country')</td>
                            </tr>
                        </table></p>
                    </a>
                @endforeach
            </div>
        @endif
        </p>
    </div> <!-- .col-12.col-lg-8 -->
</div> <!-- .row -->
@if($people)
    <div class="row">
        <div class="col-12">
            {{ $people->links() }}
        </div>
    </div>
@endif
@endsection
