@extends('layout')

@section('content')
<div class="row justify-center">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Edytuj informacje o osobie
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('person.update', compact('person')) }}" class="mb-0">
                    @csrf
                    @method('PUT')

                    <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Nazwisko*</label>
                        <div class="col-md-6">
                            <input id="name" type="text" name="name" value="{{ old('name') ?? $person->name }}"
                                   class="form-control @error('name') is-invalid @enderror"  autofocus>
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <label for="birth_date" class="col-md-4 col-form-label text-md-right">Data urodzenia</label>
                        <div class="col-md-6">
                            <input id="birth_date" type="text" class="form-control @error('birth_date') is-invalid @enderror"
                            name="birth_date" placeholder="YYYY-MM-DD" value="{{ old('birth_date') ?? ($person->birth_date ? $person->birth_date->format('Y-m-d') : '')}}" >
                            @error('birth_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <label for="death_date" class="col-md-4 col-form-label text-md-right">Data śmierci</label>
                        <div class="col-md-6">
                            <input id="death_date" type="text" class="form-control @error('death_date') is-invalid @enderror"
                            name="death_date" placeholder="YYYY-MM-DD" value="{{ old('death_date') ?? $person->death_date ? $person->death_date->format('Y-m-d') : ''}}" >
                            @error('death_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">                        
                        <label for="birth_country" class="col-md-4 col-form-label text-md-right">Kraj pochodzenia</label>
                        <div class="col-md-6">
                            <select name="birth_country" class="custom-select @error('birth_country') is-invalid @enderror">
                                <option>brak</option>
                                @foreach(\App\Country::orderBy('name', 'ASC')->get() as $c)
                                    <option value="{{ $c->id }}"
                                        @php
                                            if (old('birth_country') == false && $person->country && $c->id == $person->country->id) {
                                                echo 'selected';
                                            }
                                            else if ($c->id == old('birth_country')) {
                                                echo 'selected';
                                            }
                                        @endphp
                                        >{{ $c->name }}</option>
                                @endforeach
                            </select>
                            @error('birth_country')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <label for="birth_city" class="col-md-4 col-form-label text-md-right">Miejsce urodzenia</label>
                        <div class="col-md-6">
                            <input id="birth_city" type="text" name="birth_city" value="{{ old('birth_city') ?? $person->birth_city }}"
                                    class="form-control @error('birth_city') is-invalid @enderror"  >
                            @error('birth_city')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}
    
                    <div class="form-group row">
                        <label for="height" class="col-md-4 col-form-label text-md-right">Wzrost</label>
                        <div class="col-md-6">
                            <input id="height" type="text" name="height" value="{{ old('height') ?? $person->height }}"
                                   class="form-control @error('height') is-invalid @enderror"
                                   placeholder="cm"  >
                            @error('height')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <label for="bio" class="col-md-4 col-form-label text-md-right">Notka biograficzna</label>
                        <div class="col-md-6">
                            <textarea class="form-control @error('bio') is-invalid @enderror"
                                      id="bio" name="bio" rows="4"
                                      placeholder="Maks. 2048 znaków">{{ old('bio') ?? $person->bio }}</textarea>
                            @error('bio')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <div class="col-6 col-md-3 offset-md-4">
                            <button type="submit" class="btn btn-primary">Aktualizuj</button>
                        </div> {{-- .col-md-8.offset-md-4 --}}
                        <div class="col-6 col-md-3 text-right">* Pole wymagane</div>
                    </div> {{-- .form-group.row --}}
                </form>
            </div> {{-- .card-body --}}
        </div> {{-- .card --}}
    </div> {{-- .col-12.col-lg-8 --}}
</div> {{-- .row --}}
<div class="row justify-center mt-4">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Usuń wpis
            </div>
            <div class="card-body">
                <p>Czy na pewno chcesz usunąć wpis tej osoby z bazy? Ta operacja jest nieodwracalna.</p>
                <form method="POST" action="{{ route('person.destroy', compact('person')) }}" class="mb-0">
                    @csrf
                    @method('delete')

                    <div class="form-group row">
                        <div class="col-md-3 offset-md-4">
                            <button type="submit" class="btn btn-danger">Usuń</button>
                            <a class="btn btn-light" href="{{ route('person.show', compact('person')) }}">Wróć</a>
                        </div> {{-- .col-md-8.offset-md-4 --}}
                    </div> {{-- .form-group.row --}}
                </form>
            </div> {{-- .card-body --}}
        </div> {{-- .card --}}
    </div> {{-- .col-12.col-lg-8 --}}
</div> {{-- .row --}}
@endsection
