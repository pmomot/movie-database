@extends('layout')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Człowiek kina
            </div>
            <div class="card-body">
                <h5 class="card-title">{{ $person->name }}</h5>
                <p class="mb-2">
                    <table>
                        <tr class="align-top">
                            <td class="text-muted">Data urodzenia:</td>
                            <td class="pl-4">@include('person.birth-date')</td>
                        </tr>
                        @if($person->isDead())
                        <tr class="align-top">
                            <td class="text-muted">Data śmierci:</td>
                            <td class="pl-4">@include('person.death-date')</td>
                        </tr>
                        @endif
                        <tr class="align-top">
                            <td class="text-muted">Miejsce urodzenia:</td>
                            <td class="pl-4">@include('person.born-in')<td>
                        </tr>
                        <tr class="align-top">
                            <td class="text-muted">Wzrost:</td>
                            <td class="pl-4">
                                @isset($person->height)
                                    {{ $person->height}} cm
                                @else
                                    Brak informacji
                                @endisset
                            </td>
                        </tr>
                    </table>
                </p>
                <p class="card-text">
                    @isset($person->bio)
                        {{ $person->bio }}
                    @else
                        <span class="font-italic">Brak notki biograficznej</span>
                    @endisset
                </p>
                <a href="{{ route('person.edit', compact('person')) }}" class="card-link">Edytuj / Usuń</a>
            </div>
          </div>
    </div> {{-- .col-12.col-lg-8 --}}
</div> {{-- .row --}}
<div class="row">
    <div class="col-12 mt-3">
        <div class="card">
            <div class="card-header">
                Role
            </div>
            <div class="card-body">
                @if($person->performances->isEmpty())
                    Niczego jeszcze nie dodano.
                @else
                    <table class="table table-hover mb-0">
                        <tbody>
                            @foreach($person->performances as $performance)
                                <tr>
                                    <td>
                                        <a href="{{ route('movie.show', ['movie' => $performance->movie]) }}" class="text-reset">{{ $performance->movie->title }}</a>
                                        <a href="{{ route('movie.show', ['movie' => $performance->movie]) }}" class="ml-2 text-muted">{{ $performance->character }}</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div> {{-- .card-body --}}
        </div> {{-- .card --}}
    </div> {{-- .col-12 --}}
</div> {{-- .row --}}
@endsection
