@php

$born_in = [];

if (isset($person->birth_city)) {
    $born_in[] = $person->birth_city;
}

if (isset($person->country)) {
    $born_in[] = $person->country->name;
}

if (empty($born_in)) {
    echo "Brak informacji";
}
else {
    echo implode(", ", $born_in);
}

@endphp
