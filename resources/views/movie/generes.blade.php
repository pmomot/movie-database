@empty($countries = $movie->generes->pluck("name")->all())
    <span class="font-italic text-muted">Brak informacji</span>
@else
    {{ implode(', ', $countries) }}
@endempty