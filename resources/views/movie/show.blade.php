@extends('layout')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Film
            </div>
            <div class="card-body">
                <h5 class="card-title">{{ $movie->title }}</h5>
                <p class="mb-2">
                    <table>
                        <tr class="align-top">
                            <td class="text-muted">Gatunek:</td>
                            <td class="pl-4">
                                @include('movie.generes')
                                <a class="ml-2" href="{{ route('movie-genere.index', compact('movie')) }}">Edytuj</a>
                            </td>
                        </tr>
                        <tr class="align-top">
                            <td class="text-muted">Kraj:</td>
                            <td class="pl-4">
                                @include('movie.countries')
                                <a class="ml-2" href="{{ route('movie-country.index', compact('movie')) }}">Edytuj</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-muted">Premiera:</td><td class="pl-4">{{  \App\Helpers\localizeDate($movie->release_date)  }}</td>
                        </tr>
                    </table>
                </p>
                <p class="card-text">
                    @isset($movie->summary)
                        {{ $movie->summary }}
                    @else
                        <span class="font-italic">Brak opisu</span>
                    @endisset
                </p>
                <a href="{{ route('movie.edit', compact('movie')) }}" class="card-link">Edytuj</a>
            </div>
          </div>
    </div> {{-- .col-12 --}}
    <div class="col-12 mt-3">
        <div class="card">
            <div class="card-header">
                Obsada
            </div>
            <div class="card-body">
	        <p>
                    <a href=" {{ route('performance.selectActor', compact('movie')) }}" class="btn btn-primary">Dodaj</a>
                </p>
                @if($movie->cast->isEmpty())
                    Brak informacji o obsadzie.
                @else
                    <table class="table table-hover mb-0">
                        <tbody>
                            @foreach($movie->cast as $performance)
                                <tr>
                                    <td>
                                        <a href="{{ route('person.show', ['person' => $performance->actor]) }}" class="text-reset">{{ $performance->actor->name }}</a>
                                        <a href="{{ route('performance.edit', compact('performance')) }}" class="ml-2 text-muted">{{ $performance->character }}</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div> {{-- .card-body --}}
        </div> {{-- .card --}}
    </div> {{-- .col-12 --}}
</div> {{-- .row --}}
@endsection
