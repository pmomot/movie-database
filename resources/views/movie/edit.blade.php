@extends('layout')

@section('content')
<div class="row justify-center">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Edytuj szczegóły filmu
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('movie.update', compact('movie')) }}" class="mb-0">
                    @csrf
                    @method('PUT')

                    <div class="form-group row">
                    <label for="title" class="col-md-4 col-form-label text-md-right">Tytuł</label>
                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') ?? $movie->title }}" required autofocus>
                            @error('title')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <label for="release_date" class="col-md-4 col-form-label text-md-right">Premiera</label>
                        <div class="col-md-6">
                            <input id="release_date" type="text" class="form-control @error('release_date') is-invalid @enderror"
                            name="release_date" placeholder="YYYY-MM-DD" value="{{ old('release_date') ?? $movie->release_date->format('Y-m-d') }}" required>
                            @error('release_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <label for="summary" class="col-md-4 col-form-label text-md-right">Opis</label>
                        <div class="col-md-6">
                            <textarea class="form-control @error('summary') is-invalid @enderror"
                                id="summary" name="summary" rows="4"
                                placeholder="Maks. 255 znaków">{{ old('summary') ?? $movie->summary }}</textarea>
                            @error('summary')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">Aktualizuj</button>
                        </div> {{-- .col-md-8.offset-md-4 --}}
                    </div> {{-- .form-group.row --}}
                </form>
            </div> {{-- .card-body --}}
        </div> {{-- .card --}}
    </div> {{-- .col-12.col-lg-8 --}}
</div> {{-- .row --}}
@endsection
