@extends('layout')

@section('content')
<div class="row justify-center">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Nowy Film
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('movie.store') }}" class="mb-0">
                    @csrf

                    <div class="form-group row">
                    <label for="title" class="col-md-4 col-form-label text-md-right">Tytuł*</label>
                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autofocus>
                            @error('title')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <label for="release_date" class="col-md-4 col-form-label text-md-right">Premiera*</label>
                        <div class="col-md-6">
                            <input id="release_date" type="text" class="form-control @error('release_date') is-invalid @enderror"
                            name="release_date" placeholder="YYYY-MM-DD" value="{{ old('release_date') }}" required>
                            @error('release_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <label for="summary" class="col-md-4 col-form-label text-md-right">Opis</label>
                        <div class="col-md-6">
                            <textarea class="form-control @error('summary') is-invalid @enderror"
                                      id="summary" name="summary" rows="4"
                                      placeholder="Maks. 255 znaków">{{ old('summary') }}</textarea>
                            @error('summary')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <div class="col-6 col-md-3 offset-md-4">
                            <button type="submit" class="btn btn-primary">Dodaj</button>
                        </div> {{-- .col-md-8.offset-md-4 --}}
                        <div class="col-6 col-md-3 text-right">* Pole wymagane</div>
                    </div> {{-- .form-group.row --}}
                </form>
            </div> {{-- .card-body --}}
        </div> {{-- .card --}}
    </div> {{-- .col-12.col-lg-8 --}}
</div> {{-- .row --}}
@endsection
