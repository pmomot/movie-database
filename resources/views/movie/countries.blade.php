@empty($generes = $movie->countries->pluck("name")->all())
    <span class="font-italic text-muted">Brak informacji</span>
@else
    {{ implode(', ', $generes) }}
@endempty
